import Foundation

struct Properties {
    var ogcFid: Int
    var obrId1: URL
    var druh: PossibleKind
    var nazev: String
}
